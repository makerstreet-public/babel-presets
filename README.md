# @touchtribe/babel-presets

[![NPM version][npm-image]][npm-url]

Touchtribe’s company-wide set of Babel transforms.

## Usage

Install this package, as well as the parts of Babel you wish to use:

**With Yarn**

```bash
yarn add @babel/core @touchtribe/babel-presets
```

**With npm**

```bash
npm install @babel/core @touchtribe/babel-presets
```

Then, in your Babel configuration , set this package as the babel preset you’d like to use:

```json
{
  "babel": {
    "presets": ["@touchtribe/babel-presets/web"]
  }
}
```

## Presets

### @touchtribe/babel-presets

Default preset which includes:

```json
{
  "presets": [
    [ "@babel/preset-env", {
      "useBuiltIns": "usage"
    }]
  ],
  "plugins": [
    "@babel/plugin-proposal-class-properties",
    "@babel/plugin-proposal-export-default-from",
    "@babel/plugin-proposal-export-namespace-from",
    "@babel/plugin-proposal-nullish-coalescing-operator",
    "@babel/plugin-proposal-optional-chaining",
    "@babel/plugin-syntax-dynamic-import"
  ]
}
```

Accepts the following options:
- `isDebug`: Will set `@babel/preset-env`'s `debug` option. *Default: false*
- `targets`: Will set `@babel/preset-env`'s `targets` option. *Default: undefined*
- `alias`: Will set `babel-plugin-module-resolver` alias option. *Default: undefined*

**Usage**
```js
{
  "presets": [
    ['@touchtribe/babel-presets', {
      // ["@babel/preset-env", {debug: isDebug || false }],
      "isDebug": true,
      
      // ["@babel/preset-env", {debug: isDebug || false }],
      "targets": [
        "> 1%"
      ],
      
      // ["babel-plugin-module-resolver", { alias: alias }] 
      "alias": {
        "app": "./src/app"
      }
    }]
  ]
}
```

### @touchtribe/babel-presets/web

Preset for websites, includes `@touchtribe/babel-presets` with default `targets`

Accepts the following options:
- `isLegacy`: Will set targets to default legacy-browsers
- `targets`: Will set `@babel/preset-env`'s `targets` option. *Default: defaultTargets*

**defaultTargets**
```
{
  "esmodules": false,
  "browsers" : [
    'last 2 Chrome versions',
    'not Chrome < 60',
    'last 2 Safari versions',
    'not Safari < 10.1',
    'last 2 iOS versions',
    'not iOS < 10.3',
    'last 2 Firefox versions',
    'not Firefox < 54',
    'last 2 Edge versions',
    'not Edge < 15'
  ]
}
```
**legacyTargets**
```
{
  "esmodules": false,
  "browsers": [
    '> 1%',
    'last 2 versions',
    'Firefox ESR'
  ]
}
```
**Usage**
```js
{
  "presets": [
    ['@touchtribe/babel-presets/web', {
      "isLegacy": true
    }]
  ]
}
```

### @touchtribe/babel-presets/node

Preset for websites, includes `@touchtribe/babel-presets` with default `targets`

Accepts the following options:
- `targets`: Will set `@babel/preset-env`'s `targets` option. *Default: nodeTargets*
- `modules`: Will set `@babel/preset-env`'s `modules` options. *Default: false*

**nodeTargets**
```
{
  node: 'current'
}
```
**Usage**
```js
{
  "presets": [
    ['@touchtribe/babel-presets/node', {
      "modules": "cjs"
    }]
  ]
}
```


### @touchtribe/babel-presets/react

Convenience preset for React. includes `@babel/preset-react`
- `debug` is set to `true` when in `development` environment

```js
{
  "presets": [
    ['@touchtribe/babel-presets/node'],
    ['@touchtribe/babel-presets/react']
  ]
}
```

[npm-url]: https://npmjs.org/package/@touchtribe/babel-presets
[npm-image]: http://img.shields.io/npm/v/@touchtribe/babel-presets.svg?style=flat-square
