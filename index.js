const { declare } = require('@babel/helper-plugin-utils')
const { getEnv, ifUndefined, validateBoolOption } = require('./utils')
const { testTargets, presetEnvConfig, defaultPlugins } = require('./config')

function createPreset (api, options) {
  const { isTest } = getEnv()

  const presetEnvOptions = {
    ...presetEnvConfig,

    debug: validateBoolOption('isDebug', options.isDebug, false),
    useBuiltIns: ifUndefined(options.useBuiltIns, presetEnvConfig.useBuiltIns),
    corejs: ifUndefined(options.corejs, presetEnvConfig.corejs),

    targets: isTest
      ? testTargets
      : options.targets,

    modules: options.modules
  }

  let plugins = defaultPlugins

  if (typeof options.alias === 'object') {
    plugins = [
      ...plugins,
      ['module-resolver', {
        alias: options.alias
      }]
    ]
  }

  return {
    presets: [
      ['@babel/preset-env', presetEnvOptions]
    ],
    plugins
  }
}

module.exports = declare((api, options) => {
  api.assertVersion(7)

  return createPreset(api, options)
})
