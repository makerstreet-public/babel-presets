function test (props) {
  const { name, ...restProps } = props
  const [user1, user2] = ['name 1', 'name 2']
  return {
    name,
    user1,
    user2,
    ...restProps
  }
}

const testFn = () => true

const testFn2 = () => {
  return false
}
