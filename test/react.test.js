'use strict'
const { transform } = require('./test-utils')

let webPreset = require('../web')
let nodePreset = require('../node')
let reactPreset = require('../react')

const testFiles = [
  'react-fragment',
  'react-jsx'
]
const defaultPresets = [
  ['@touchtribe/babel-presets/web|react', [[webPreset, {}], [reactPreset, {}]]],
  ['@touchtribe/babel-presets/web|react:legacy', [[webPreset, { isLegacy: true }], [reactPreset, {}]]],
  ['@touchtribe/babel-presets/node|react', [[nodePreset, {}], [reactPreset, {}]]],
]
defaultPresets.forEach((entry) => {
  const [
    testTitle,
    presets
  ] = entry
  describe(testTitle, () => {
    for (let fileName of testFiles) {
      test(fileName.replace(/-/g, ' '), (done) => {
        transform(fileName, presets)
          .then(output => {
            expect(output).toMatchSnapshot()
            done()
          })
      })
    }
  })
})

