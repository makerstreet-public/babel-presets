const { declare } = require('@babel/helper-plugin-utils')
const { getEnv } = require('./utils')

function createPreset (api, options) {
  const { isDevelopment } = getEnv()

  return {
    presets: [
      ['@babel/preset-react', {
        debug: isDevelopment,
        ...options
      }]
    ]
  }
}

module.exports = declare((api, options) => {
  api.assertVersion(7)

  return createPreset(api, options)
})
